<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{!! isset($config['title']) ?  $config['title'] : 'Swagger' !!}</title>
    <link rel="stylesheet" type="text/css" href="{{ swagger_asset('swagger-ui.css') }}">
    <link rel="icon" type="image/png" href="{{ swagger_asset('favicon-32x32.png') }}" sizes="32x32"/>
    <link rel="icon" type="image/png" href="{{ swagger_asset('favicon-16x16.png') }}" sizes="16x16"/>
    <style>
        html {
            box-sizing: border-box;
            overflow: -moz-scrollbars-vertical;
            overflow-y: scroll;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        body {
            margin: 0;
            background: #fafafa;
        }
    </style>
</head>

<body>
<div id="swagger-ui"></div>
<script src="{{ swagger_asset('swagger-ui-bundle.js') }}"></script>
<script src="{{ swagger_asset('swagger-ui-standalone-preset.js') }}"></script>
<script>
    window.onload = function () {
        // Build a system
        const ui = SwaggerUIBundle({
            dom_id: '#swagger-ui',
            url: "{!! $urlToDocs !!}",
            operationsSorter: {!! isset($config['operationsSorter']) ? '"' . $config['operationsSorter'] . '"' : 'alpha' !!},
            configUrl: {!! isset($config['configUrl']) ? '"' . $config['configUrl'] . '"' : 'null' !!},
            validatorUrl: {!! isset($config['validatorUrl']) ? '"' . $config['validatorUrl'] . '"' : 'null' !!},
            oauth2RedirectUrl: {!! isset($config['oauth2RedirectUrl']) ? '"' . $config['oauth2RedirectUrl'] . '"' : '' !!},
            requestInterceptor: function (request) {
                request.headers['X-CSRF-TOKEN'] = '{{ csrf_token() }}';
                return request;
            },
            presets: [
                SwaggerUIBundle.presets.apis,
                SwaggerUIStandalonePreset
            ],
            plugins: [
                SwaggerUIBundle.plugins.DownloadUrl
            ],
            layout: {!! isset($config['layout']) ? '"' . $config['layout'] . '"' : 'StandaloneLayout' !!},
            docExpansion: {!! isset($config['docExpansion']) ? '"' . $config['docExpansion'] . '"' : 'none' !!},
            deepLinking: true,
            filter: {!! isset($config['filter']) && $config['filter'] ? 'true' : 'false' !!},
            persistAuthorization: "{!! isset($config['persistAuthorization']) && $config['persistAuthorization'] ? 'true' : 'false' !!}",

        })

        window.ui = ui

        @if(in_array('oauth2', array_column(isset($config['securitySchemes'])?$config['securitySchemes']:[], 'type')))
        ui.initOAuth({
            usePkceWithAuthorizationCodeGrant: "{!! isset($config['usePkceWithAuthorizationCodeGrant']) && $config['usePkceWithAuthorizationCodeGrant'] ? 'true' : 'false' !!}"
        })
        @endif
    }
</script>
</body>
</html>
