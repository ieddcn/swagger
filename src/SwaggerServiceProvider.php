<?php

namespace Ieddcn\Swagger;

use Illuminate\Support\ServiceProvider;
use Ieddcn\Swagger\Console\GenerateDocsCommand;
use Ieddcn\Swagger\Http\Controllers\SwaggerAssetController;
use Ieddcn\Swagger\Http\Middleware\Authorize;
use Illuminate\Support\Facades\Route;

class SwaggerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCommands();
        $this->registerPublishing();

        if (!config('swagger.enabled')) {
            return;
        }
        $this->registerRoutes();
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'swagger-ui');
    }

    /**
     * 注册路由
     */
    private function registerRoutes()
    {
        Route::middlewareGroup('swagger', config('swagger.middleware', []));
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
        });
    }

    /**
     * 获取路由组配置数组
     * @return array
     */
    private function routeConfiguration()
    {
        return [
            'namespace' => 'Ieddcn\Swagger\Http\Controllers',
            'middleware' => 'swagger'
        ];
    }

    /**
     * 注册发布
     */
    private function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                base_path('vendor/swagger-api/swagger-ui/dist') => public_path('vendor/swagger'),
            ], ['swagger-assets']);

            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/swagger-ui'),
            ], 'swagger-views');

            $this->publishes([
                __DIR__ . '/../config/swagger.php' => config_path('swagger.php'),
            ], 'swagger-config');

            $this->publishes([
                __DIR__.'/../stubs/SwaggerServiceProvider.stub' => app_path('Providers/SwaggerServiceProvider.php'),
            ], 'swagger-provider');
        }
    }

    /**
     * 注册command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\GenerateDocsCommand::class,
                Console\InstallCommand::class,
            ]);
        }
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/swagger.php', 'swagger'
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @codeCoverageIgnore
     *
     * @return array
     */
    public function provides()
    {
        return [
            'swagger:generate',
        ];
    }
}
