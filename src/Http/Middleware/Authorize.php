<?php

namespace Ieddcn\Swagger\Http\Middleware;

 use Ieddcn\Swagger\Swagger;

class Authorize
{
    // /**
    //  * Handle the incoming request.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \Closure  $next
    //  * @return \Illuminate\Http\Response
    //  */
    public function handle($request, $next)
    {
         return Swagger::check($request) ? $next($request) : abort(403);
    }
}
