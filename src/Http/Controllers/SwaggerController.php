<?php

namespace Ieddcn\Swagger\Http\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request as RequestFacade;
use Illuminate\Support\Facades\Response as ResponseFacade;
use Ieddcn\Swagger\GeneratorFactory;
use Illuminate\Support\Facades\Route;

class SwaggerController extends BaseController
{
    /**
     * @var GeneratorFactory
     */
    protected $generatorFactory;

    public function __construct(GeneratorFactory $generatorFactory)
    {
        $this->generatorFactory = $generatorFactory;
    }

    /**
     * Dump api-docs content endpoint. Supports dumping a json, or yaml file.
     *
     * @param Request $request
     * @param  ?string $file
     * @return Response
     *
     * @throws Exception
     * @throws FileNotFoundException
     */
    public function docs(Request $request)
    {
        $fileSystem = new Filesystem();
        $targetFile = $request->offsetGet('jsonFile');
        // 获取配置名称
        $pos = strpos($targetFile, '-api-docs');
        $config = config('swagger.docs.' . substr($targetFile, 0, $pos));
        $yaml = pathinfo($targetFile)['extension'] === 'yaml' ? true : false;

        $filePath = storage_path('docs/' . $targetFile);
        if ($config['generate_always']) {
            $generator = $this->generatorFactory->make($config, $targetFile);
            $generator->generateDocs();
        }

        if (!$fileSystem->exists($filePath)) {
            abort(404, sprintf('文件不存在: "%s"', $filePath));
        }

        $content = $fileSystem->get($filePath);
        if ($yaml) {
            return ResponseFacade::make($content, 200, [
                'Content-Type' => 'application/yaml',
                'Content-Disposition' => 'inline',
            ]);
        }

        return ResponseFacade::make($content, 200, [
            'Content-Type' => 'application/json',
        ]);
    }

    /**
     * 显示swagger页面
     *
     * @param Request $request
     * @return Response
     */
    public function show($params = null)
    {
        $name = Route::currentRouteName();
        $config = config('swagger.docs.' . $name);
        $urlToDocs = $this->generateDocumentationFileURL($name, $config);
        return ResponseFacade::make(
            view($params === 'redoc'? 'swagger-ui::docs' : 'swagger-ui::index', [
                'config' => $config,
                'urlToDocs' => $urlToDocs,
            ]),
            200
        );
    }

    /**
     * 显示Oauth2页面
     *
     * @param Request $request
     * @return string
     *
     * @throws Exception
     * @throws FileNotFoundException
     */
    public function oauth2Callback(Request $request)
    {
        $fileSystem = new Filesystem();
        $documentation = $request->offsetGet('documentation');
        return $fileSystem->get(swagger_ui_dist_path($documentation, 'oauth2-redirect.html'));
    }

    /**
     * 生成swagger 文件地址
     * @param string $documentation
     * @param array $config
     * @return string
     */
    protected function generateDocumentationFileURL(string $name, array $config)
    {

        $fileUsedForDocs = $name . '-api-docs.';
        $format = $config['format'] ?? 'json';
        $fileUsedForDocs .= $format == 'yaml' ? 'yaml' : 'json';
        return route('swagger-docs', $fileUsedForDocs);
    }
}
