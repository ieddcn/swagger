<?php

use Illuminate\Support\Facades\Route;
use Ieddcn\Swagger\Http\Controllers\SwaggerController;
use Ieddcn\Swagger\Http\Controllers\SwaggerAssetController;

Route::get('/vendor/swagger/{asset}', [SwaggerAssetController::class, 'index'])->name('swaagger-assets');

foreach (config('swagger.docs', []) as $routePath => $docItem) {
    Route::get($routePath . '/{redoc?}', [SwaggerController::class, 'show'])->name($routePath);
    Route::get($routePath . '/oauth2_callback', [SwaggerController::class, 'oauth2Callback'])->name('swagger-callback');
}

Route::get('/docs/{jsonFile}', [SwaggerController::class, 'docs'])->name('swagger-docs');
