<?php

namespace Ieddcn\Swagger\Console;

use Illuminate\Console\Command;
use Ieddcn\Swagger\GeneratorFactory;

class GenerateDocsCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swagger:generate {doc?} {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '生成swagger文档';

    /**
     * Execute the console command.
     *
     * @param GeneratorFactory $generatorFactory
     * @return void
     *
     * @throws Exception
     */
    public function handle(GeneratorFactory $generatorFactory)
    {

        $all = $this->option('all');
        if ($all) {
            $documentations = array_keys(config('swagger.docs', []));

            foreach ($documentations as $doc) {
                $this->generateDocumentation($generatorFactory, $doc);
            }

            return;
        }

        $doc = $this->argument('doc');

        if (!$doc) {
            throw new \Exception('请指定要发布的配置文档');
        };

        $this->generateDocumentation($generatorFactory, $doc);
    }

    /**
     * @param GeneratorFactory $generatorFactory
     * @param string $documentation
     *
     * @throws Exception
     */
    private function generateDocumentation(GeneratorFactory $generatorFactory, string $doc)
    {
        $this->info('开始生成: ' . $doc);
        $config = config('swagger.docs.' . $doc, []);
        $targetFile = $doc . '-api-docs.' . ($config['format'] ?? 'json');
        $generator = $generatorFactory->make($config, $targetFile);
        $generator->generateDocs();
        $this->info('生成成功: ' . $doc);
    }
}
