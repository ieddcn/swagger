<?php

namespace Ieddcn\Swagger\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swagger:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'swagger文档安装';

    /**
     * Execute the console command.
     * @return void
     * @throws Exception
     */
    public function handle()
    {

        $this->comment('发布 Swagger Service Provider...');
        $this->callSilent('vendor:publish', ['--tag' => 'swagger-provider']);

        $this->comment('发布 Swagger 资源文件...');
        $this->callSilent('vendor:publish', ['--tag' => 'swagger-assets']);

        $this->comment('发布 Swagger 展示页面...');
        $this->callSilent('vendor:publish', ['--tag' => 'swagger-views']);

        $this->comment('发布 Swagger 配置文件...');
        $this->callSilent('vendor:publish', ['--tag' => 'swagger-config']);

        $this->registerSwaggererviceProvider();

        $this->info('Swagger 安装成功.');
    }

    /**
     * 注册 Swaagger service provider 到应用的配置文件中.
     *
     * @return void
     */
    protected function registerSwaggererviceProvider()
    {
        $namespace = Str::replaceLast('\\', '', $this->laravel->getNamespace());

        $appConfig = file_get_contents(config_path('app.php'));

        if (Str::contains($appConfig, $namespace.'\\Providers\\SwaggerServiceProvider::class')) {
            return;
        }

        $lineEndingCount = [
            "\r\n" => substr_count($appConfig, "\r\n"),
            "\r" => substr_count($appConfig, "\r"),
            "\n" => substr_count($appConfig, "\n"),
        ];

        $eol = array_keys($lineEndingCount, max($lineEndingCount))[0];

        file_put_contents(config_path('app.php'), str_replace(
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol,
            "{$namespace}\\Providers\RouteServiceProvider::class,".$eol."        {$namespace}\Providers\SwaggerServiceProvider::class,".$eol,
            $appConfig
        ));

        file_put_contents(app_path('Providers/SwaggerServiceProvider.php'), str_replace(
            "namespace App\Providers;",
            "namespace {$namespace}\Providers;",
            file_get_contents(app_path('Providers/SwaggerServiceProvider.php'))
        ));
    }
}
