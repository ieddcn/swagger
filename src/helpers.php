<?php

if (!function_exists('swagger_ui_dist_path')) {
    /**
     * Returns swagger-ui composer dist path.
     *
     * @param string $documentation
     * @param null $asset
     * @return string
     *
     * @throws Exception
     */
    function swagger_ui_dist_path($asset)
    {
        $allowed_files = [
            'favicon-16x16.png',
            'favicon-32x32.png',
            'oauth2-redirect.html',
            'swagger-ui-bundle.js',
            'swagger-ui-bundle.js.map',
            'swagger-ui-standalone-preset.js',
            'swagger-ui-standalone-preset.js.map',
            'swagger-ui.css',
            'swagger-ui.css.map',
            'swagger-ui.js',
            'swagger-ui.js.map',
        ];

        $path = base_path('vendor/swagger-api/swagger-ui/dist/');

        if (!$asset) {
            return realpath($path);
        }

        if (!in_array($asset, $allowed_files)) {
            throw new Exception(sprintf('(%s) - 文件禁止访问', $asset));
        }

        return realpath($path . $asset);
    }
}

if (!function_exists('swagger_asset')) {
    /**
     * Returns asset from swagger-ui composer package.
     *
     * @param string $documentation
     * @param $asset string
     * @return string
     *
     * @throws Exception
     */
    function swagger_asset($asset)
    {
        $file = swagger_ui_dist_path($asset);
        if (!file_exists($file)) {
            throw new Exception(sprintf('swagger 静态资源不存在', $asset));
        }
        return route('swaagger-assets', $asset) . '?v=' . md5_file($file);
    }
}
