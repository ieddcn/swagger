<?php

namespace Ieddcn\Swagger;

use Illuminate\Support\Facades\Facade;

class SwaggerFacade extends Facade
{
    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Generator::class;
    }
}
