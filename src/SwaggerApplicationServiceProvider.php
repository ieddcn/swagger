<?php

namespace Ieddcn\Swagger;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class SwaggerApplicationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->authorization();
    }

    /**
     * Configure the Swagger authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();

        Swagger::auth(function ($request) {
            return app()->environment('local') ||
                   Gate::check('viewSwagger', [$request->user()]);
        });
    }

    /**
     * Register the Swagger gate.
     *
     * This gate determines who can access Swagger in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewSwagger', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
