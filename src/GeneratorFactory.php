<?php

namespace Ieddcn\Swagger;


class GeneratorFactory
{
    /**
     * @var ConfigFactory
     */
    private $configFactory;

    public function __construct(ConfigFactory $configFactory)
    {
        $this->configFactory = $configFactory;
    }

    /**
     * Make Generator Instance.
     *
     * @param string $documentation
     * @return Generator
     * @throws Exception
     */
    public function make(array $config, string $file_name): Generator
    {
        $config = $this->configFactory->documentationConfig($config);
        $yamlCopyRequired = $config['generate_yaml_copy'] ?? false;
        $secSchemesConfig = $config['securitySchemes'] ?? [];
        $secConfig = $config['security'] ?? [];
        $security = new SecurityDefinitions($secSchemesConfig, $secConfig);
        return new Generator($file_name, $config, $yamlCopyRequired, $security);
    }
}
