<?php
use Ieddcn\Swagger\Http\Middleware\Authorize;

return [
    /**
     * 是否开启swagger
     */
    'enabled' => env('SWAGGER_ENABLED', true),

    /**
     * swagger 访问授权
     */
    'middleware' => [
        'web',
        Authorize::class
    ],

    /**
     * 文档配置，
     * swagger 访问路径 /swagger
     */
    'docs' => [
        'swagger' => [
            'title' => 'swagger',
            'format' => 'json',  // 文档类型 [json,yaml]
            'scan' => [ // 扫描路径
                base_path('app'),
            ],
            // 是否每次打开自动生成
            'generate_always' => env('SWAGGER_GENERATE_ALWAYS', false),
            // 下面为swagger ui 配置，详情搜：SwaggerUIBundle
            'operationsSorter' => 'alpha', // 排序方式 [alpha,method]
            'configUrl' => '', // 附加配置url
            'validatorUrl' => '', // 指定 OpenAPI 规范验证器的 URL
            'oauth2RedirectUrl' => '', // 授权地址
            'docExpansion' => '', // 默认展开状态 [none,list,full]
            'filter' => true, // 是否开启筛选
            'persistAuthorization' => false, // 是否开启持久授权
            'usePkceWithAuthorizationCodeGrant' => false, // 启用或禁用 OAuth 2.0 授权码授权流程中的 PKCE
            'layout' => 'StandaloneLayout', // Swagger UI 的布局风格，可选值包括 'BaseLayout' 和 'StandaloneLayout'。
            // swagger-php 扫描配置
            'scanOptions' => [
                /**
                 * 分析者: defaults to \OpenApi\StaticAnalyser .
                 * @see \OpenApi\scan
                 */
                'analyser' => null,

                /**
                 * analysis: defaults to a new \OpenApi\Analysis .
                 * 默认为分析 \OpenApi\analysis
                 * @see \OpenApi\scan
                 */
                'analysis' => null,

                /**
                 * 自定义查询路径处理类。
                 * @link https://github.com/zircote/swagger-php/tree/master/Examples/schema-query-parameter-processor
                 * @see \OpenApi\scan
                 */
                'processors' => [
                    // new \App\SwaggerProcessors\SchemaQueryParameter(),
                ],

                /**
                 * 要扫描的文件模式（默认值：*.php）
                 * @see \OpenApi\scan
                 */
                'pattern' => null,

                /*
                 * 排除的扫描目录
                 * @see \OpenApi\scan
                */
                'exclude' => [],

                /*
                 * 设置OpenAPI版本 3.0.0 or OpenAPI 3.1.0
                 */
                'open_api_spec_version' => env('SWAGGER_OPEN_API_SPEC_VERSION', '3.0.0'),
            ],
            /*
             * 可用于注释的常量配置
             */
            'constants' => [
                'MIT' => 'Ieddcn Swagger',
            ],
            /*
             * 是否生成 yaml 副本
             */
            'generate_yaml_copy' => env('SWAGGER_GENERATE_YAML_COPY', false),
            'swagger_base_path' => env('SWAGGER_BASE_PATH', null),
            // 定义安全方案的类型和参数
            'securitySchemes' => [
                // 示例写法
                'token' => [
                    'type' => 'apiKey',
                    'description' => '授权Token',
                    'name' => 'token',
                    'in' => 'header',
                ]
            ],
            // 全局安全策略
            'security' => [
                [
                    // 示例写法
                    'token' => []
                ]
            ],
        ],

    ],
];
