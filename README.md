## Ieddcn/swagger

### 安装
```bash
composer require Ieddcn/swagger
```
### 完成安装
```bash
php artisan swagger:install
```

### 生成文档 - 指定文档
```bash
php artisan swagger:generate swagger
```

### 生成文档 - 全部
```bash
php artisan swagger:generate --all
```
